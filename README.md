# DeathStarBench Traces

This dataset consists of API calls to the social network DeathStarBench microbenchmark.

These are *not* Zipkin traces -- we added additional instrumentation to collect X-Trace traces, which capture finer-grained dependencies.

To visualize a trace, download the JSON file for the trace, then view it in [this](http://people.mpi-sws.org/~jcmace/xtrace/swimlane.html) visualization.

For questions, e-mail jcmace@mpi-sws.org

How to cite (BibTeX):

```
@Misc{anand2019deathstarbenchtraces,
  title = {{X-Trace trace dataset for DeathStarBench}},
  author = {Anand, Vaastav and Mace, Jonathan},
  year = {2019},
  howpublished = {Retrieved October 2019 from \url{https://gitlab.mpi-sws.org/cld/trace-datasets/deathstarbench_traces/tree/master/socialNetwork}}
}
```